package com.vazhasapp.assignment10

interface OnClickPager {
    fun onPagerClick(itemPosition: Int)
}
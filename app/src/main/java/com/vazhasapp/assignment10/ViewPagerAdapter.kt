package com.vazhasapp.assignment10

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter

class ViewPagerAdapter(private val context: Context, private val listener: OnClickPager) :
    PagerAdapter() {

    override fun instantiateItem(collection: ViewGroup, position: Int): Any {

        val currentCharacterPhoto = GalleryEnum.values()[position]

        val layout =
            LayoutInflater.from(context)
                .inflate(R.layout.gallery_layout, collection, false)

        collection.addView(layout)

        layout.findViewById<ImageView>(R.id.imGallery)
            .setImageResource(currentCharacterPhoto.getImageId())

        layout.setOnClickListener {
            listener.onPagerClick(position)
        }

        return layout
    }

    override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
        collection.removeView(view as View)
    }

    override fun getCount() = GalleryEnum.values().size

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }
}
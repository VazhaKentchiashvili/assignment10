package com.vazhasapp.assignment10

enum class GalleryEnum(private val mLayout: Int) {
    SAURON(R.mipmap.sauron),
    LEGOLAS(R.mipmap.legolas),
    GIMLI(R.mipmap.gimli),
    GANDALF(R.mipmap.gandalf),
    ARAGORN(R.mipmap.aragorn);

    fun getImageId(): Int {
        return mLayout
    }
}
package com.vazhasapp.assignment10.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.vazhasapp.assignment10.GalleryEnum
import com.vazhasapp.assignment10.OnClickPager
import com.vazhasapp.assignment10.ViewPagerAdapter
import com.vazhasapp.assignment10.databinding.FragmentMainBinding

class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
    }

    private fun init() {
        // Setup ViewPager Adapter
        binding.myPager.adapter = ViewPagerAdapter(requireContext(), object : OnClickPager {
            override fun onPagerClick(itemPosition: Int) {
                showFullPhoto(itemPosition)
            }
        })
    }

    // Open and send data in ShowFullPhotoFragment
    private fun showFullPhoto(position: Int) {
        val myImage = GalleryEnum.values()[position].getImageId()
        val direction = MainFragmentDirections.actionMainFragmentToShowFullPhotoFragment(myImage)
        findNavController().navigate(direction)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}